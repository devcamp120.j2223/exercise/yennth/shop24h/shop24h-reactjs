import { createStore, combineReducers } from "redux";
import CartEvents from "../components/action/CartEvents";
import OrderEvent from "../components/action/OrderEvent";
import FilterEvent from "../components/action/FilterEvent";

const appReducer = combineReducers({
  filterReducer: FilterEvent,
  cartReducer: CartEvents,
  orderReducer: OrderEvent
});

const store = createStore(appReducer, undefined, undefined);
export default store;
