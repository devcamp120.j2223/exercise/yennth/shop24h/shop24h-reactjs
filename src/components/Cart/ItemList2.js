import { Button, Grid, IconButton, Checkbox, TableContainer, TableBody, Table, TableHead, TableRow, TableCell,
     Paper, Avatar, Typography, Snackbar, Alert } from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch } from "react-redux";
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { useNavigate } from "react-router-dom";
import { auth } from "../../firebase";
function ItemsList2() {
    const navigate = useNavigate();
    const { products, totalPrice, totalItem, buyList } = useSelector((reduxData) => reduxData.cartReducer);
    const dispatch = useDispatch();

    const [user, setUser] = useState(null);
    useEffect(() => {
        auth.onAuthStateChanged((result) => {
            setUser(result);
        });
    }, []);
    const onDeleteProductHandler = (productInfo, productIndex) => {
        dispatch({
            type: "DELETE_ITEM",
            value: {
                productInfo: productInfo,
                productIndex: productIndex
            }
        })
    }
    const [numSelected, setNumSelected] = useState(0);
    const [selected, setSelected] = useState([]);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const increaseItem = (product) => {
        dispatch({
            type: "ADD_ITEM",
            value: {
                quantity: 1,
                itemInfo: product,
            },
        });
    }
    const decreaseItem = (item, itemIndex) => {
        dispatch({
            type: "DECREASE_ITEM",
            value: {
                item: item,
                itemIndex: itemIndex
            },
        });
    }

    const onSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelecteds = products.map((item) => item._id);
            setSelected(newSelecteds);
            setNumSelected(products.length);
            selectAllItem();
            return;
        }
        setSelected([]);
        setNumSelected(0);
        removeAllItem();
    }
    const selectedHandle = (id) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };
    const isSelected = (productId) => selected.indexOf(productId) !== -1;

    const selectAllItem = () => {
        dispatch({
            type: "SELECT_ALL_ITEM",
        });
    }
    const removeAllItem = () => {
        dispatch({
            type: "REMOVE_ALL_ITEM",
        });
    }
    const onCheckClick = (event, product) => {
        if (event.target.checked === true) {
            setNumSelected(numSelected + 1);
            dispatch({
                type: "ADD_TO_BUY_LIST",
                value: {
                    product: product
                },
            });
        } else {
            setNumSelected(numSelected - 1)
            dispatch({
                type: "REMOVE_FROM_BUY_LIST",
                value: {
                    product: product
                },
            });
        }
        selectedHandle(product._id)
    }
    function formatCash(cash) {
        if (cash)
            return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    }
    const onBuyClick = () => {
        if (user === null) {
            navigate('/login')
        }
        else if (buyList.length) {
            navigate(`/order`);
        }
        else{
            setIsOpenAlert(true);
        }
    }
    return (
        <>
            <Grid container mt={2} sx={{ display: { xs: 'none', sm: 'flex' } }}>
                <TableContainer component={Paper}  >
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead style={{ "backgroundColor": "#f1f3f4" }}>
                            <TableRow>
                                <TableCell align="left">
                                    <Checkbox
                                        indeterminate={numSelected > 0 && numSelected < products.length}
                                        checked={products.length > 0 && numSelected === products.length}
                                        onClick={onSelectAllClick}
                                    />
                                </TableCell>
                                <TableCell align="left">Products</TableCell>
                                <TableCell align="left">Unit Price</TableCell>
                                <TableCell align="left">Quantity</TableCell>
                                <TableCell align="left">Total Price</TableCell>
                                <TableCell align="left">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                products.map((product, index) => {
                                    const isItemSelected = isSelected(product._id);
                                    return (
                                        <TableRow key={index} >
                                            <TableCell >
                                                <Checkbox
                                                    checked={isItemSelected}
                                                    onClick={(event) => onCheckClick(event, product)}
                                                />
                                            </TableCell>
                                            <TableCell >
                                                <Grid container>
                                                    <Avatar
                                                        src={product.imageUrl}
                                                        alt={`img ${index}`}
                                                        variant="square"
                                                        sx={{ width: 56, height: 56, marginRight: "10px" }}
                                                    />
                                                    {product.name}
                                                </Grid>

                                            </TableCell>
                                            <TableCell >{formatCash(product.promotionPrice)}</TableCell>
                                            <TableCell >
                                                <IconButton onClick={() => { decreaseItem(product, index) }} color="primary" aria-label="remove a product" style={{ height: '40px', width: '40px' }}>
                                                    <RemoveIcon />
                                                </IconButton>
                                                {product.quantity}
                                                <IconButton onClick={() => { increaseItem(product) }} color="primary" aria-label="add a product" style={{ height: '40px', width: '40px' }}>
                                                    <AddIcon />
                                                </IconButton>
                                            </TableCell>

                                            <TableCell >{formatCash(product.promotionPrice * product.quantity)}</TableCell>
                                            <TableCell >
                                                <IconButton onClick={() => { onDeleteProductHandler(product, index) }}>
                                                    <DeleteIcon />
                                                </IconButton>
                                            </TableCell>
                                        </TableRow>
                                    )
                                })
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            <Grid container textAlign={'center'} justifyContent={'center'} mt={2} sx={{ display: { xs: 'flex', sm: 'none' } }}>
                <Grid item xs={2}>
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < products.length}
                        checked={products.length > 0 && numSelected === products.length}
                        onClick={onSelectAllClick}
                    />
                </Grid>
                <Grid item xs={6}>
                    <Typography variant="h5">Sản phẩm</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography variant="h5">Chi Tiết</Typography>
                </Grid>
                <Grid item xs={12}>
                    {
                        products.map((product, index) => {
                            const isItemSelected = isSelected(product._id);
                            return (
                                <Grid container key={index} my={3}>
                                    <Grid item xs={2} >
                                        <Checkbox
                                            checked={isItemSelected}
                                            onClick={(event) => onCheckClick(event, product)}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Grid container justifyContent={'center'}>
                                            <Avatar
                                                src={product.imageUrl}
                                                alt={`img ${index}`}
                                                variant="square"
                                                sx={{ width: 56, height: 56 }}
                                            />
                                        </Grid>
                                        <Grid container justifyContent={'center'} >
                                            <Typography fontWeight="bold">{product.name}</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={4} >
                                        <Grid container justifyContent={'center'}>
                                            <IconButton onClick={() => { decreaseItem(product, index) }} color="primary" aria-label="remove a product" style={{ height: '40px', width: '40px' }}>
                                                <RemoveIcon />
                                            </IconButton>
                                            {product.quantity}
                                            <IconButton onClick={() => { increaseItem(product) }} color="primary" aria-label="add a product" style={{ height: '40px', width: '40px' }}>
                                                <AddIcon />
                                            </IconButton>
                                        </Grid>
                                        <Grid container justifyContent={'center'}>
                                            <Typography variant="h6">{formatCash(product.promotionPrice * product.quantity)}</Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                              
                            )
                        })
                    }
                </Grid>

            </Grid>
            <Grid container mt={1}>
                <Grid item xs={12} textAlign="end">
                    <Typography variant="h5" sx={{ display: "inline", marginRight: "15px", color: "orange", fontWeight: "bold" }}>Total ({totalItem} item): {formatCash(totalPrice)}</Typography>
                    <Button variant="contained" color="error" onClick={onBuyClick} >Đặt hàng</Button>
                </Grid>
            </Grid>
            <Snackbar open={isOpenAlert} autoHideDuration={6000} onClose={()=>{setIsOpenAlert(false)}}>
                <Alert onClose={()=>{setIsOpenAlert(false)}} severity={"warning"} sx={{ width: '100%' }}>
                    Đơn hàng chưa có sản phẩm nào!!!
                </Alert>
            </Snackbar>
        </>

    );
}
export default ItemsList2;
