import React, { Component } from "react";
import Slider from "react-slick";
import tea from "../../assets/images/tea.jpg"
import coffee from "../../assets/images/coffee4.jpg"
import juice from "../../assets/images/juice.png"


function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", right: "0", }}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", left: "0", zIndex: "2" }}
            onClick={onClick}
        />
    );
}

const items = [
    {
        src: tea,
        altText: 'Slide 1',
        caption: 'Slide 1',
        key: 1
    },
    {
        src: coffee,
        altText: 'Slide 2',
        caption: 'Slide 2',
        key: 2
    },
    {
        src: juice,
        altText: 'Slide 3',
        caption: 'Slide 3',
        key: 3
    }
];

export default class CarouselSlider extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <div style={{ textAlign: "center" }}>
                <Slider {...settings} >
                    {
                        items.map((item, index) => {
                            return (

                                <img src={item.src} alt={item.altText} key={index} style={{ maxHeight: "600px" }} />

                            )
                        })
                    }
                </Slider>
            </div>
        );
    }
}