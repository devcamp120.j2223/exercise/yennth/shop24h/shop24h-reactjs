import { Grid } from "@mui/material";
import CarouselSlider from "./CarouselSlider";
import LastestProducts from "./LastestProducts";
import ViewAll from "./ViewAll";

function Content() {
    return (
        <Grid>
            <CarouselSlider />
            <LastestProducts />
            <ViewAll />
        </Grid>
    )
}
export default Content;