import { Grid, Button } from "@mui/material";
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
function ViewAll() {
  return (
    <Grid textAlign="center">
      <Button variant="contained" color="error" href="/products">
        XEM THÊM
        <ArrowDropDownIcon />
      </Button>
    </Grid>
  );
}
export default ViewAll;
