import { Grid, Typography } from "@mui/material";
function ProductFooter() {
    return (
        <Grid container>
            <Grid item xs={12} md={12} sm={12} lg={12}>
                <Typography variant="h6" fontWeight="bold" style={{ "color": "#8f5741" }} >
                    SẢN PHẨM
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} >
                <Typography style={{ "color": "#8f5741" }}>
                    Trà
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Sinh tố
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Cà phê
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Nước ép
                </Typography>
            </Grid>
        </Grid>
    )
}
export default ProductFooter;