import { Grid, Typography } from "@mui/material";
function ServiceFooter() {
    return (
        <Grid container>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography variant="h6" fontWeight="bold" >
                    ĐIỀU KHOẢN
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Chính sách bảo mật thông tin
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Chính sách giao hàng
                </Typography>
            </Grid>
        </Grid>
    )
}
export default ServiceFooter;