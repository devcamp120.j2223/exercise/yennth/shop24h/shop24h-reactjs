import { Grid, Typography } from "@mui/material";
function SupportFooter() {
    return (
        <Grid container>
            <Grid item xs={12} md={12} sm={12} lg={12}>
                <Typography variant="h6" fontWeight="bold" style={{ "color": "#8f5741" }}>
                    LIÊN HỆ
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Địa chỉ: 000 Đồng Khởi, Q1, tp HCM
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Số điện thoại: 0909000909
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12} style={{ "color": "#8f5741" }}>
                <Typography>
                    Email: larita@gmail.com
                </Typography>
            </Grid>
        </Grid>
    )
}
export default SupportFooter;