
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import { useState } from 'react';
import logo from "../../assets/images/logo7.png"
import IconNavBar from './IconNavBar';
const pages = [
    {
        name: "SẢN PHẨM",
        url: "/products"
    },
    {
        name: "GIỎ HÀNG",
        url: "/cart"
    }
]


function Header() {
    const [anchorElNav, setAnchorElNav] = useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    return (
        <AppBar position="static" sx={{ backgroundColor: "#a9e2a8" }}>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <IconButton
                        size="large"
                        href="/"
                        sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }}
                    >
                        <img alt="logo" src={logo} style={{ width: "50px" }} />
                    </IconButton>
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href="/"
                        sx={{
                            mr: 2,
                            display: { xs: 'none', md: 'flex' },
                            fontWeight: 700,
                            // letterSpacing: '.3rem',
                            color: '#8f5741',
                            textDecoration: 'none',
                            fontFamily:"Satisfy,cursive",
                        }}
                    >
                        MEAINU
                    </Typography>

                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                        >
                            <MenuIcon />
                            {/* <img alt="logo" src={logo} style={{ width: "50px" }} /> */}
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            {pages.map((page, index) => (
                                <MenuItem key={index} onClick={handleCloseNavMenu}>
                                    <Typography
                                        textAlign="center"
                                        component="a"
                                        href={page.url}
                                        sx={{                               
                                            fontWeight: 700,
                                            color: '#8f5741',
                                            textDecoration: 'none',
                                        }}
                                    >
                                        {page.name}
                                    </Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                    {/* <IconButton
                        size="large"
                        sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }}
                    >
                        <img alt="logo" src={logo} style={{ width: "50px" }} />
                    </IconButton> */}
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href="/"
                        sx={{
                            mr: 2,
                            display: { xs: 'flex', md: 'none' },
                            flexGrow: 1,
                            fontWeight: 700,
                            fontFamily:"Satisfy,cursive",
                            letterSpacing: '.3rem',
                            color: '#8f5741',
                            textDecoration: 'none',
                        }}
                    >
                         MEAINU
                    </Typography>
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {pages.map((page, index) => (
                            <Button
                                key={index}
                                href={page.url}
                                // onClick={handleCloseNavMenu}
                                sx={{     
                                    my: 2,                          
                                    fontWeight: 700,
                                    color: '#8f5741',
                                    textDecoration: 'none',
                                    display: 'block'
                                }}
                            >
                                {page.name}
                            </Button>
                        ))}
                    </Box>
                    <Box sx={{ flexGrow: 0 }}>
                        <IconNavBar sx={{ p: 0 }} />
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>

    );
}
export default Header;
