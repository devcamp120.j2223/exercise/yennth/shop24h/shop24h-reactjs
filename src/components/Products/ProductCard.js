import {
  Grid,
  Card,
  CardContent,
  Typography,
  CardActionArea,
  CardMedia
} from "@mui/material";
import { useNavigate } from "react-router-dom";

function ProductCard({ product }) {
  const navigate = useNavigate();
  const hrefProductDetail = `/products/${product._id}`;
  const onCardClick = () => {
    navigate(hrefProductDetail);
  };
  function formatCash(cash) {
    if (cash)
      return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }
  return (
    <Grid item xs={12} md={3} sm={6} lg={3} py={3} px={3} style={{ display: "flex", textAlign: "center", justifyContent: "center" }} >
      <Card sx={{ width: "100%", height: "100%" }}>
        <CardActionArea onClick={onCardClick}>
          <CardMedia
            component="img"
            height="194"
            image={product.imageUrl}
            alt={product.name}
          />
          <CardContent>

            <Typography gutterBottom variant="h6" component="div">
              {product.name}
            </Typography>
            <Typography variant="h5" color="red">
              {formatCash(product.promotionPrice)} Đ
            </Typography>
            {
              product.promotionPrice < product.buyPrice ?
                <Typography
                  variant="h6"
                  color="gray"
                  sx={{ textDecorationLine: "line-through" }}
                >
                  {formatCash(product.buyPrice)} Đ
                </Typography>
                : null
            }

          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}
export default ProductCard;
