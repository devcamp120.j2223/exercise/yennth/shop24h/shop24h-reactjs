import {
    Grid,
    FormControlLabel,
    Typography,
    TextField,
    Button,
    Radio,
    RadioGroup,
    FormControl,
    Drawer,
    Box,
    Container
} from "@mui/material";
import { useEffect, useState } from "react";

import { useSelector, useDispatch } from "react-redux";
import FilterAltIcon from '@mui/icons-material/FilterAlt';
function ProductFilter2() {
    const [isOpenDrawer, setIsOpenDrawer] = useState(false);
    const dispatch = useDispatch();
    const {
        taskFilterNameInput,
        taskFilterProductType,
        taskFilterMaxPriceInput,
        taskFilterMinPriceInput,
    } = useSelector((reduxData) => reduxData.filterReducer);
    const [productTypes, setProductTypes] = useState([]);
    const getData = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    };
    useEffect(() => {
        getData(
            `http://localhost:8000/productTypes`
        )
            .then((data) => {
                console.log(data.data);
                setProductTypes(data.data);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);
    const inputProductChangeHandler = (event) => {
        dispatch({
            type: "TASK_INPUT_PRODUCT_CHANGE",
            value: event.target.value,
        });
    };
    const inputMaxPriceChangeHandler = (event) => {
        dispatch({
            type: "TASK_MAX_PRICE_PRODUCT_CHANGE",
            value: event.target.value,
        });
    };
    const inputMinPriceChangeHandler = (event) => {
        dispatch({
            type: "TASK_MIN_PRICE_PRODUCT_CHANGE",
            value: event.target.value,
        });
    };
    const onBtnFilterClick = () => {
        dispatch({
            type: "TASK_ON_FILTER_CLICK",
        });
        closeDrawer();
    };
    const productTypeChangeHandler = (event) => {
        dispatch({
            type: "TASK_PRODUCT_TYPE_CHANGE",
            value: event.target.value,
        });
    };
    const closeDrawer = () => {
        setIsOpenDrawer(false);
    }
    const openDrawer = () => {
        setIsOpenDrawer(true);
    }
    return (
        <div >
            <Grid 
            container
             textAlign={{ xs: "center", md: "end", sm: "end" }}
              paddingRight={{ xs: 1, md: 5, sm: 5 }}
              sx={{display: { xs: 'flex', md: 'none' }}}
              >
                <Grid item xs={12} md={12} sm={12} lg={12}>
                    <Button
                        onClick={openDrawer}
                        variant="contained"
                        color="secondary">
                        <FilterAltIcon />
                        Lọc Sản Phẩm
                    </Button>
                </Grid>
            </Grid>

            <Drawer
                anchor="right"
                open={isOpenDrawer}
                onClose={closeDrawer}
            >
                <Box
                    sx={{ width: 280 }}
                    role="presentation"
                >
                    <Grid container p={2} >
                        <Grid item xs={12} md={12} sm={12} lg={12}>
                            <Typography gutterBottom variant="h6" component="div">
                                Lọc theo tên
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} lg={12}>
                            <TextField
                                size="small"
                                fullWidth
                                label="nhập tên sản phẩm"
                                onChange={inputProductChangeHandler}
                                value={taskFilterNameInput}
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} lg={12} mt={2}>
                            <Typography gutterBottom variant="h6" component="div">
                                Lọc theo giá tiền
                            </Typography>
                        </Grid>

                        <Grid item xs={12} md={12} sm={12} lg={12}>
                            <TextField
                                fullWidth
                                size="small"
                                label="Min"
                                type="number"
                                value={taskFilterMinPriceInput}
                                onChange={inputMinPriceChangeHandler}
                            />
                        </Grid>

                        <Grid item xs={12} md={12} sm={12} lg={12}>
                            <Typography
                                gutterBottom
                                variant="h6"
                                component="div"
                                textAlign="center"
                            >
                                -
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} lg={12}>
                            <TextField
                                size="small"
                                fullWidth
                                label="Max"
                                type="number"
                                value={taskFilterMaxPriceInput}
                                onChange={inputMaxPriceChangeHandler}
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} lg={12} mt={2}>
                            <Typography gutterBottom variant="h6" component="div">
                                Lọc theo loại sản phẩm
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} lg={12}>
                            <FormControl>
                                <RadioGroup
                                    value={taskFilterProductType}
                                    name="radio-buttons-group"
                                    onChange={productTypeChangeHandler}
                                >
                                    <FormControlLabel value="" control={<Radio />} label="None" />
                                    {
                                        productTypes.map((type, index) => (
                                            <FormControlLabel
                                                value={type._id}
                                                key={index}
                                                control={<Radio />}
                                                label={type.name}
                                            />
                                        ))
                                    }
                                </RadioGroup>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} lg={12} mt={2}>
                            <Button
                                color="secondary"
                                variant="contained"
                                fullWidth
                                sx={{ fontWeight: "bold" }}
                                onClick={onBtnFilterClick}
                            >
                                Tìm
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </Drawer>
            <Container sx={{display: { xs: 'none', md: 'flex' }, marginTop:5}} >
                <Grid
                    container
                    spacing={3}
                    justifyContent="center"
                    display='flex'
                    px={2}
                    sx={{ border: "double #ff6347", borderRadius: "10px", backgroundColor: "#fff" }}
                >
                    <Grid item xs={4}>
                        <Grid container justifyContent="center">
                            <Typography gutterBottom variant="h6" component="div">
                                Lọc theo giá tiền
                            </Typography>
                        </Grid>
                        <Grid container>
                            <Grid item xs={5}>
                                <TextField
                                    fullWidth
                                    size="small"
                                    label="Min"
                                    type="number"
                                    value={taskFilterMinPriceInput}
                                    onChange={inputMinPriceChangeHandler}
                                />
                            </Grid>

                            <Grid item xs={2}>
                                <Typography
                                    gutterBottom
                                    variant="h6"
                                    component="div"
                                    textAlign="center"
                                >
                                    -
                                </Typography>
                            </Grid>
                            <Grid item xs={5}>
                                <TextField
                                    size="small"
                                    fullWidth
                                    label="Max"
                                    type="number"
                                    value={taskFilterMaxPriceInput}
                                    onChange={inputMaxPriceChangeHandler}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={4} textAlign="center">
                        <Grid container justifyContent="center">
                            <Typography gutterBottom variant="h6" component="div">
                                Lọc theo loại sản phẩm
                            </Typography>
                        </Grid>
                        <Grid container>
                            <RadioGroup
                                row
                                value={taskFilterProductType}
                                name="radio-buttons-group"
                                onChange={productTypeChangeHandler}
                                sx={{ justifyContent: "center" }}
                            >
                                <FormControlLabel value="" control={<Radio />} label="None" />
                                {
                                    productTypes.map((type, index) => (
                                        <FormControlLabel
                                            value={type._id}
                                            key={index}
                                            control={<Radio />}
                                            label={type.name}
                                        />
                                    ))
                                }
                            </RadioGroup>
                        </Grid>
                    </Grid>
                    <Grid item xs={2}>
                        <Typography gutterBottom variant="h6" component="div">
                            Lọc theo tên
                        </Typography>
                        <Grid container>
                            <TextField
                                size="small"
                                fullWidth
                                label="nhập tên sản phẩm"
                                onChange={inputProductChangeHandler}
                                value={taskFilterNameInput}
                            />
                        </Grid>
                    </Grid>
                    <Grid item xs={2} my="auto">
                        <Button
                            color="secondary"
                            variant="contained"
                            fullWidth
                            sx={{ fontWeight: "bold" }}
                            onClick={onBtnFilterClick}
                        >
                            Tìm
                        </Button>
                    </Grid>
                </Grid>
            </Container>



        </div>



    );
}
export default ProductFilter2;
