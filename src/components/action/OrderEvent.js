const initialState = {
  fullname: "",
  email: "",
  phoneNumber: "",
  address: "",
  note: "",
};

function OrderEvent(state = initialState, action) {

  switch (action.type) {
    case "GET_USER_INFOMATION":
      const user = action.value;

      if (user.displayName) {
        state.fullname = user.displayName;
      };
      if (user.fullName) {
        state.fullname = user.fullName;
      };
      if (user.email) {
        state.email = user.email;
      };
      if (user.phoneNumber) {
        state.phoneNumber = user.phoneNumber;
      };
      if (user.phone) {
        state.phoneNumber = user.phone;
      };
      if (user.address) {
        state.address = user.address;
      };
      return {
        ...state,
      };
    case "CHANGE_FULLNAME_INPUT":
      return {
        ...state,
        fullname: action.value
      }
    case "CHANGE_ADDRESS_INPUT":
      return {
        ...state,
        address: action.value
      }
    case "CHANGE_EMAIL_INPUT":
      return {
        ...state,
        email: action.value
      }
    case "CHANGE_PHONE_NUMBER_INPUT":
      return {
        ...state,
        phoneNumber: action.value
      }
    case "CHANGE_NOTE_INPUT":
      return {
        ...state,
        note: action.value
      }
    default:
      return state;
  }
};

export default OrderEvent;
