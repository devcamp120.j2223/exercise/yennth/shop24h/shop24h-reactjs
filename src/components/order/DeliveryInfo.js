
import { Grid, TextField, Typography } from "@mui/material";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { auth } from "../../firebase";
function DeliveryInfo() {
    const dispatch = useDispatch();
    const { fullname, email, address, phoneNumber, note } = useSelector((reduxData) => reduxData.orderReducer);
    useEffect(() => {
        auth.onAuthStateChanged((result) => {
            getUserInfo(result);
        });
    }, [])
    const callApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    };
    const getUserInfo = (user) => {
        let email = user.email;
        callApi("http://localhost:8000/customers?email=" + email)
            .then((data) => {
                if (data.data.length > 0) {
                    dispatch({
                        type: "GET_USER_INFOMATION",
                        value: data.data[0]
                    })
                }
                else {
                    dispatch({
                        type: "GET_USER_INFOMATION",
                        value: user
                    })
                }
            })
            .catch((error) => {
                console.log(error);
            })

    }
    const changeFullnameHandler = (event) => {
        dispatch({
            type: "CHANGE_FULLNAME_INPUT",
            value: event.target.value
        })
    }
    const changeAddressHandler = (event) => {
        dispatch({
            type: "CHANGE_ADDRESS_INPUT",
            value: event.target.value
        })
    }
    const changeEmailHandler = (event) => {
        dispatch({
            type: "CHANGE_EMAIL_INPUT",
            value: event.target.value
        })
    }
    const changeNoteHandler = (event) => {
        dispatch({
            type: "CHANGE_NOTE_INPUT",
            value: event.target.value
        })
    }
    const changePhoneNumberHandler = (event) => {
        dispatch({
            type: "CHANGE_PHONE_NUMBER_INPUT",
            value: event.target.value
        })
    }

    return (
        <div>
            <Grid container mt={1} spacing={2} p={3} sx={{ border: "double #ff6347", borderRadius: "10px", backgroundColor: "#fff" }}>
                <Grid item xs={12} sm={12} lg={12} md={12}>
                    <Typography variant="h5">
                        THÔNG TIN GIAO HÀNG
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} lg={12} md={12}>
                    <TextField size="small" fullWidth label="Full name" value={fullname} id="fullname" onChange={changeFullnameHandler} />
                </Grid>
                <Grid item xs={12} sm={12} lg={12} md={12}>
                    <TextField size="small" fullWidth label="Phone Number" value={phoneNumber} onChange={changePhoneNumberHandler} />
                </Grid>
                <Grid item xs={12} sm={12} lg={12} md={12}>
                    <TextField size="small" fullWidth label="Email" value={email} onChange={changeEmailHandler} />
                </Grid>
                <Grid item xs={12} sm={12} lg={12} md={12}>
                    <TextField size="small" fullWidth label="Address" value={address} onChange={changeAddressHandler} />
                </Grid>
                <Grid item xs={12} sm={12} lg={12} md={12}>
                    <TextField size="small" fullWidth label="Note" value={note} onChange={changeNoteHandler} />
                </Grid>
            </Grid>

        </div>
    );
}

export default DeliveryInfo;
