
import { Container, Grid, Typography, Button, Alert, Snackbar } from "@mui/material";
import { useState } from "react";

import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import DeliveryInfo from "./DeliveryInfo";
import OrderProduct from "./OrderProduct";

function OrderDetail() {
  const dispatch = useDispatch();
  const { totalPrice, buyList } = useSelector((reduxData) => reduxData.cartReducer);
  const { fullname, email, address, phoneNumber, note } = useSelector((reduxData) => reduxData.orderReducer);
  const [alertContent, setAlertContent] = useState("");
  const [isOpenAlert, setIsOpenAlert] = useState(false);
  const [alertStatus, setAlertStatus] = useState('warning');
  const navigate = useNavigate();
  const formatCash = (cash) => {
    if (cash)
      return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }

  const onBtnConfirmClick = () => {
    const isCheck = validateData();
    if (isCheck) {
      createNewOrderHandler();
    }
  }
  const validateData = () => {
    if (fullname === "") {
      setIsOpenAlert(true);
      setAlertContent("Full name is invalid");
      setAlertStatus("warning");
      return false;
    }
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailRegex.test(email)) {
      setIsOpenAlert(true);
      setAlertContent("Email is invalid");
      setAlertStatus("warning");
      return false;
    }
    if (isNaN(phoneNumber) || phoneNumber === "") {
      setIsOpenAlert(true);
      setAlertContent("Phone Number is invalid");
      setAlertStatus("warning");
      return false;
    }
    if (address === "") {
      setIsOpenAlert(true);
      setAlertContent("address is invalid");
      setAlertStatus("warning");
      return false;
    }
    return true
  }
  const handleCloseAlert = () => {
    setAlertContent("");
    setIsOpenAlert(false);
    if (alertStatus === "success") {
      navigate("/");
    }
  }
  const callApi = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions);
    const responseData = await response.json();
    return responseData;
  };
  const createOrderForNewCustomer = () => {
    console.log("create-customer")
    let body = {
      method: "POST",
      body: JSON.stringify({
        fullName: fullname,
        phone: phoneNumber,
        email: email,
        address: address,

      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      }
    }
    callApi('http://localhost:8000/customers', body)
      .then((data) => {
        if (data.data) {
          callApiCreateOrder(data.data)
        }
        else {
          setIsOpenAlert(true);
          setAlertContent(data.message);
          setAlertStatus("error");
        }
      })
      .catch((error) => {
        setIsOpenAlert(true);
        setAlertContent(error);
        setAlertStatus("error");
      })
  }
  const callApiCreateOrder = (customer) => {
    const customerId = customer._id;
    let body = {
      method: "POST",
      body: JSON.stringify({
        orderDetail: buyList,
        cost: totalPrice,
        note: note,
        customerInfo: customer
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      }
    }
    callApi(`http://localhost:8000/customers/${customerId}/orders`, body)
      .then((data) => {
        setIsOpenAlert(true);
        setAlertContent("Order placed successfully");
        setAlertStatus("success");
        dispatch({
          type: "BUY_SUCCESS",
        })
      })
      .catch((error) => {
        console.log(error)
        setIsOpenAlert(true);
        setAlertContent(error);
        setAlertStatus("error");
      })
  }
  const createNewOrderHandler = () => {
    callApi("http://localhost:8000/customers?phoneNumber=" + phoneNumber)
      .then((data) => {
        if (data.data.length === 0) {
          createOrderForNewCustomer();
        } else {
          callApiCreateOrder(data.data[0]);
        }
      })
      .catch((error) => {
        console.log(error);
        setIsOpenAlert(true);
        setAlertContent(error);
        setAlertStatus("error");
      })
  }
  return (
    <div>
      <Container>
        <OrderProduct />
        <DeliveryInfo />
        <Grid container mt={1} spacing={2} p={3} sx={{ border: "double #ff6347", borderRadius: "10px", backgroundColor: "#fff" }}>
          <Grid item xs={12} sm={6} lg={6} md={6}>
            <Typography>Tổng thanh toán: <span style={{ color: "orange", fontWeight: "bold", fontSize: "x-large" }}>{formatCash(totalPrice)} VND</span></Typography>
          </Grid>
          <Grid item xs={12} sm={6} lg={6} md={6} textAlign="end">
            <Button variant="contained" color="warning" size="large" onClick={onBtnConfirmClick}>ĐẶT HÀNG</Button>
          </Grid>
        </Grid>
      </Container>
      <Snackbar open={isOpenAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={alertStatus} sx={{ width: '100%' }}>
          {alertContent}
        </Alert>
      </Snackbar>
    </div>
  );
}

export default OrderDetail;
