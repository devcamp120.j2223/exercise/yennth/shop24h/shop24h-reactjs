
import { Grid, Typography, Avatar, TableCell, TableHead, Paper, Table, TableRow, TableBody, TableContainer } from "@mui/material";
import { useSelector } from "react-redux";
function OrderProduct() {
    const { buyList } = useSelector((reduxData) => reduxData.cartReducer);
    function formatCash(cash) {
        if (cash)
            return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    }
    return (
        <Grid container mt={1} spacing={2} p={3} sx={{ border: "double #ff6347", borderRadius: "10px", backgroundColor: "#fff" }}>
            <Grid item xs={12} sm={12} lg={12} md={12}>
                <Typography variant="h5">
                    Thông tin đơn hàng
                </Typography>
            </Grid>
            <Grid item xs={12} sm={12} lg={12} md={12}>
                <TableContainer component={Paper} sx={{ display: { xs: 'none', sm: 'flex' } }}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">Products</TableCell>
                                <TableCell align="left">Unit Price</TableCell>
                                <TableCell align="left">Quantity</TableCell>
                                <TableCell align="left">Item Subtotal</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                buyList.map((product, index) => {
                                    return (
                                        <TableRow key={index} >
                                            <TableCell >
                                                <Grid container>
                                                    <Avatar
                                                        src={product.imageUrl}
                                                        alt={`img ${index}`}
                                                        variant="square"
                                                        sx={{ width: 56, height: 56, marginRight: "10px" }}
                                                    />
                                                    {product.name}
                                                </Grid>
                                            </TableCell>
                                            <TableCell >{formatCash(product.promotionPrice)}</TableCell>
                                            <TableCell >
                                                {product.quantity}
                                            </TableCell>
                                            <TableCell >{formatCash(product.promotionPrice * product.quantity)}</TableCell>
                                        </TableRow>
                                    )
                                })
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid container textAlign={'center'} justifyContent={'center'} mt={2} sx={{ display: { xs: 'flex', sm: 'none' } }}>
                    <Grid item xs={12}>
                        {
                            buyList.map((product, index) => (
                                <Grid container key={index} mb={3}>
                                    <Grid item xs={4} my="auto">
                                        <Avatar
                                            src={product.imageUrl}
                                            alt={`img ${index}`}
                                            variant="square"
                                            sx={{ width: 56, height: 56, margin:"auto" }}
                                        />
                                    </Grid>
                                    <Grid item xs={8}>
                                    <Typography sx={{fontWeight:"bold"}}>{product.name}</Typography>
                                    <Typography >Số lượng: {product.quantity}</Typography>
                                    <Typography >Đơn giá:  {formatCash(product.promotionPrice)}</Typography>
                                    <Typography >Số tiền: {formatCash(product.promotionPrice * product.quantity)}</Typography>
                                    </Grid>
                                </Grid>

                            ))
                        }
                    </Grid>


                </Grid>

            </Grid>
        </Grid>
    );
}

export default OrderProduct;
