import { Container } from "@mui/material";
import ViewAll from "../Content/ViewAll";
import ProductData from "./ProductData";

import SimilarProducts from "./SimilarProducts";

function ProductDetail() {
  return (
    <Container>
      <ProductData />
      <SimilarProducts />
      <ViewAll />
    </Container>
  );
}
export default ProductDetail;
