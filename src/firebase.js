import firebase from "firebase";

import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBhatdOmDSDUlviiXlkU1HmBEe3YlGcIf8",
  authDomain: "devcamp-shop24h-56774.firebaseapp.com",
  projectId: "devcamp-shop24h-56774",
  storageBucket: "devcamp-shop24h-56774.appspot.com",
  messagingSenderId: "609467372732",
  appId: "1:609467372732:web:e06791191458071f0e2cb9"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();
