import { Container } from "@mui/material";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";

import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import ProductDetail from "../components/product-detail/ProductDetail";
function ProductInfo() {
  const breadcrumbArray = [
    {
      name: "Trang chủ",
      url: "/",
    },
    {
      name: "Sản phẩm",
      url: "/products",
    },
  ];

  return (
    <div style={{ backgroundColor: "#fdf4eb" }}>
      <Header />
      <BreadCrumb breadcrumbArray={breadcrumbArray} pageName="Chi tiết sản phẩm" />
      <Container>
        <ProductDetail />
      </Container>

      <Footer />
    </div>
  );
}

export default ProductInfo;
